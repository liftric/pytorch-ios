# pytorch-ios

Prebuild binaries of PyTorch for iOS

## Usage

Just add the xcframework via SwiftPM to your project.

As linking can't be done upfront, you have to add the following to your Xcode project under Build Settings > Linking > Other Linking Flags:

```
-force_load $(BUILT_PRODUCTS_DIR)/pytorch.a
```
