// swift-tools-version:5.4
import PackageDescription

let package = Package(
    name: "PyTorch",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "pytorch",
            targets: ["pytorch"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "pytorch",
            url: "https://gitlab.com/liftric/pytorch-ios/-/raw/main/releases/xcframework/pytorch-ios-v2.0.1.zip",
            checksum: "513791b40ab6d5534bcdf072c3e0f7db31bd757c57e54c990d7b92d7d1dfcaf9"
        )
    ]
)
