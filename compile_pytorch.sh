#!/bin/bash

git clone git@github.com:pytorch/pytorch.git
cd pytorch

git checkout v2.0.1
git submodule sync && git submodule update --init --recursive

rm -rf work/
mkdir -p work/ios-arm64/
mkdir -p work/ios-arm64-x86_64-simulator/

rm -rf build_ios/
PYTHON=python3 BUILD_PYTORCH_MOBILE=1 ENABLE_BITCODE=0 IOS_PLATFORM=OS IOS_ARCH=arm64 ./scripts/build_ios.sh
libtool build_ios/install/lib/*.a -o ./work/ios-arm64/pytorch.a

rm -rf build_ios/
PYTHON=python3 BUILD_PYTORCH_MOBILE=1 ENABLE_BITCODE=0 IOS_PLATFORM=SIMULATOR IOS_ARCH=arm64 ./scripts/build_ios.sh
libtool build_ios/install/lib/*.a -o ./work/ios-arm64-x86_64-simulator/pytorch-arm64.a

rm -rf build_ios/
PYTHON=python3 BUILD_PYTORCH_MOBILE=1 ENABLE_BITCODE=0 IOS_PLATFORM=SIMULATOR IOS_ARCH=x86_64 ./scripts/build_ios.sh
libtool build_ios/install/lib/*.a -o ./work/ios-arm64-x86_64-simulator/pytorch-x86_64.a

lipo -create -output work/ios-arm64-x86_64-simulator/pytorch.a \
  work/ios-arm64-x86_64-simulator/pytorch-arm64.a \
  work/ios-arm64-x86_64-simulator/pytorch-x86_64.a

xcodebuild -create-xcframework \
  -library work/ios-arm64/pytorch.a \
  -headers build_ios/install/include/ \
  -library work/ios-arm64-x86_64-simulator/pytorch.a \
  -headers build_ios/install/include/ \
  -output work/pytorch.xcframework
